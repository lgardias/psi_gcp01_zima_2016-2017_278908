package Hopfield_PSI_6;

import Source.Input;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Hopfield;
import org.neuroph.nnet.learning.HopfieldLearning;

import java.io.IOException;

import static java.lang.System.currentTimeMillis;

/**
 * Created by HP on 2017-01-21.
 */
public class Hopfield_PSi_6 {

    private Input data = new Input();
    private Hopfield hopfield;
    private HopfieldLearning hopfieldLearning;

    public Hopfield_PSi_6() throws IOException {
        initHopfield();
    }


    public void validateHopfield() {
        System.out.println();
        double start = currentTimeMillis();
        for (DataSetRow dataRow : data.getValidationSet().getRows()) {

            hopfield.setInput(dataRow.getInput());
            hopfield.calculate();
            hopfield.calculate();

            double[] networkOutput = hopfield.getOutput();
            System.out.println("Input: ");

            data.showArray(dataRow.getInput());

            System.out.println("Output: ");
            data.showArray(networkOutput);

        }
        double stop = System.currentTimeMillis();
        System.out.println("Execution time: " + (stop - start) + " ms");
        System.out.println();
    }

    public void initHopfield() {
        hopfield= new Hopfield(144);
        hopfieldLearning = new HopfieldLearning();

        hopfield.setLearningRule(hopfieldLearning);


        double start = currentTimeMillis();
        hopfield.learn(data.getTrainingSet());
        double stop = System.currentTimeMillis();
        System.out.println("Execution time: " + (stop - start) + " ms");

        validateHopfield();

    }

}
