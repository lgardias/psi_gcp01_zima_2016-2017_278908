package Ojj_PSI4_2;

import Source.Input;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.UnsupervisedHebbianNetwork;
import org.neuroph.nnet.learning.OjaLearning;
import org.neuroph.nnet.learning.UnsupervisedHebbianLearning;

import java.io.IOException;

/**
 * Created by HP on 2017-01-21.
 */
public class Ojj_PSI4_2 {

    private Input data = new Input();

    public Ojj_PSI4_2() throws IOException {
    }

    public void initOja() {

        UnsupervisedHebbianNetwork unsupervisedHebianNetwork = new UnsupervisedHebbianNetwork(144, 10);

        OjaLearning oja = new OjaLearning();
        oja.setLearningRate(0.5);
        oja.setMaxIterations(100);
        //oja.isPausedLearning();
		oja.addListener(new LearningEventListener() {

			public void handleLearningEvent(LearningEvent event) {
				UnsupervisedHebbianLearning oja = (UnsupervisedHebbianLearning) event.getSource();
				System.out.println("Epoka: " + oja.getCurrentIteration());
			}
		});

        unsupervisedHebianNetwork.setLearningRule(oja);

        int epoch = 1;
        do
        {
            oja.doOneLearningIteration(data.getDataPOS());
            System.out.println("Epoch " + oja.getCurrentIteration());
            //oja.doOneLearningIteration(data.getTrainingSet());
            epoch++;
        } while(oja.getCurrentIteration() > 100);




      //  unsupervisedHebianNetwork.learn(data.getPOSITIV());



    }
/*
    private void validateOja() {
        System.out.println();
        for (DataSetRow dataRow : data.getValidatingSet().getRows()) {

            unsupervisedHebianNetwork.setInput(dataRow.getInput());
            unsupervisedHebianNetwork.calculate();

            double[] networkOutput = unsupervisedHebianNetwork.getOutput();
            System.out.println("Input: ");
            data.printMatrix(dataRow.getInput());
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }
        System.out.println("Execution time: " + executionTime + " ms");
        System.out.println();

    }
    */
}
