package Source;

//import Graphics.Rysowanie;
import java.awt.image.BufferedImage;


public class Klasteryzacja {

    private static int ilosc_klastrow = 1;
    private static int rozmiar_klastra = 12;
    static Image input_image;
    public BufferedImage gui_image;
    public static Klaster[][] klastry;


    public Klasteryzacja(final Image image, BufferedImage output_image){
        input_image=image;
        gui_image=output_image;
        klastry=new Klaster[ilosc_klastrow][ilosc_klastrow];
        klasteryzujObraz();
    }

    public void klasteryzujObraz(){
        int it=0;
        for (int i=0;i<ilosc_klastrow;i++){
            for(int j=0;j<ilosc_klastrow;j++){
                klastry[i][j]=new Klaster(it,i*rozmiar_klastra,j*rozmiar_klastra);
                it++;
            }
        }
    }



}
