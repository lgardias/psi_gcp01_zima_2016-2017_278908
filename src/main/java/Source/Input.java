package Source;

import org.neuroph.core.data.DataSet;

import java.awt.geom.Arc2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by HP on 2017-01-20.
 */
public class Input {

    public Image L1,L2,L3,L4,L5,L6,L7,L8,L9,L10,L11,L12,L13,
            S1,S2,S3,S4,S5,S6,S7,S8,S9,S10;
    public Klasteryzacja klasterL1,klasterL2,klasterL3,klasterL4,klasterL5,klasterL6,
            klasterL7,klasterL8,klasterL9,klasterL10,klasterL11,klasterL12,klasterL13,
            klasterS1,klasterS2, klasterS3,klasterS4,klasterS5, klasterS6,klasterS7, klasterS8,klasterS9,klasterS10;


    public Image TS1,TS2,TS3,TS4,TS5,TS6,TS7,TS8,TS9,TS10,
                 TW1,TW2,TW3,TW4;

    public Klasteryzacja KTS1,KTS2,KTS3,KTS4,KTS5,KTS6,KTS7,KTS8,KTS9,KTS10,
            KTW1,KTW2,KTW3,KTW4;

    public Input()throws IOException{

        L1 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L1.png"));
        L2 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L2.png"));
        L3 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L3.png"));
        L4 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L4.png"));
        L5 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L5.png"));
        L6 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L6.png"));
        L7 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L7.png"));
        L8 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L8.png"));
        L9 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L9.png"));
        L10 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L10.png"));
        L11 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L11.png"));
        L12 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L12.png"));
        L13 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\L13.png"));

        S1 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S1.png"));
        S2 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S2.png"));
        S3 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S3.png"));
        S4 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S4.png"));
        S5 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S5.png"));
        S6 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S6.png"));
        S7 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S7.png"));
        S8 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S8.png"));
        S9 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S9.png"));
        S10 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\S10.png"));


        TS1 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS1.png"));
        TS2 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS2.png"));
        TS3 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS3.png"));
        TS4 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS4.png"));
        TS5 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS5.png"));
        TS6 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS6.png"));
        TS7 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS7.png"));
        TS8 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS8.png"));
        TS9 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS9.png"));
        TS10 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TS10.png"));

        TW1 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TW1.png"));
        TW2 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TW2.png"));
        TW3 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TW3.png"));
        TW4 = new Image(new File("C:\\Users\\HP\\IdeaProjects\\PSI_project\\src\\main\\resources\\Cyferki\\TW4.png"));

        int size = TS1.width;
        BufferedImage var = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
/*
        klasterL1 = new Klasteryzacja(L1, var);
        klasterL2 = new Klasteryzacja(L2, var);
        klasterL3 = new Klasteryzacja(L3, var);
        klasterL4 = new Klasteryzacja(L4, var);
        klasterL5 = new Klasteryzacja(L5, var);
        klasterL6 = new Klasteryzacja(L6, var);
        klasterL7 = new Klasteryzacja(L7, var);
        klasterL8 = new Klasteryzacja(L8, var);
        klasterL9 = new Klasteryzacja(L9, var);
        klasterL10 = new Klasteryzacja(L10, var);
        klasterL11 = new Klasteryzacja(L11, var);
        klasterL12 = new Klasteryzacja(L12, var);
        klasterL13 = new Klasteryzacja(L13, var);

        klasterS1 = new Klasteryzacja(S1, var);
        klasterS2 = new Klasteryzacja(S2, var);
        klasterS3 = new Klasteryzacja(S3, var);
        klasterS4 = new Klasteryzacja(S4, var);
        klasterS5 = new Klasteryzacja(S5, var);
        klasterS6 = new Klasteryzacja(S6, var);
        klasterS7 = new Klasteryzacja(S7, var);
        klasterS8 = new Klasteryzacja(S8, var);
        klasterS9 = new Klasteryzacja(S9, var);
        klasterS10 = new Klasteryzacja(S10, var);
*/
        KTS1 = new Klasteryzacja(TS1, var);
        KTS2 = new Klasteryzacja(TS2, var);
        KTS3 = new Klasteryzacja(TS3, var);
        KTS4 = new Klasteryzacja(TS4, var);
        KTS5 = new Klasteryzacja(TS5, var);
        KTS6 = new Klasteryzacja(TS6, var);
        KTS7 = new Klasteryzacja(TS7, var);
        KTS8 = new Klasteryzacja(TS8, var);
        KTS9 = new Klasteryzacja(TS9, var);
        KTS10 = new Klasteryzacja(TS10, var);

        KTW1 = new Klasteryzacja(TW1, var);
        KTW2 = new Klasteryzacja(TW2, var);
        KTW3 = new Klasteryzacja(TW3, var);
        KTW4 = new Klasteryzacja(TW4, var);


    }

    public double [] convert(Double [] array){

        double [] arrayOut = new double[array.length];

        for(int i = 0; i< array.length; i++){

            if(array[i] == -1){
                arrayOut[i] = 0;
            }else{
                arrayOut[i] = 1;
            }
        }

        return arrayOut;

    }

    public void showArray(double [] array){

        int var = 0;
        for(int i = 0; i < array.length; i++){

            var++;

            System.out.print((int)array[i] + " | ");
            if(var == 12){
                var=0;
                System.out.println();
            }
        }
    }

    public DataSet getAllData(){

        double [][] dataArray = new double[][]
                {
                        convert(klasterL1.klastry[0][0].colory),
                        convert(klasterL2.klastry[0][0].colory),
                        convert(klasterL3.klastry[0][0].colory),
                        convert(klasterL4.klastry[0][0].colory),
                        convert(klasterL5.klastry[0][0].colory),
                        convert(klasterL6.klastry[0][0].colory),
                        convert(klasterL7.klastry[0][0].colory),
                        convert(klasterL8.klastry[0][0].colory),
                        convert(klasterL9.klastry[0][0].colory),
                        convert(klasterL10.klastry[0][0].colory),
                        convert(klasterL11.klastry[0][0].colory),
                        convert(klasterL12.klastry[0][0].colory),
                        convert(klasterL13.klastry[0][0].colory),
                        convert(klasterS1.klastry[0][0].colory),
                        convert(klasterS2.klastry[0][0].colory),
                        convert(klasterS3.klastry[0][0].colory),
                        convert(klasterS4.klastry[0][0].colory),
                        convert(klasterS5.klastry[0][0].colory),
                        convert(klasterS6.klastry[0][0].colory),
                        convert(klasterS7.klastry[0][0].colory),
                        convert(klasterS8.klastry[0][0].colory),
                        convert(klasterS9.klastry[0][0].colory),
                        convert(klasterS10.klastry[0][0].colory)

                };
        System.out.println(dataArray[1].length);

        DataSet dataSet = new DataSet(625, 1);


        for(int i = 0; i < dataArray.length; i++){
            if( i < 13){
                dataSet.addRow(dataArray[i], new double[]{1});
            }else{
                dataSet.addRow(dataArray[i], new double[]{0});
            }

        }

        return dataSet;
    }

    public DataSet getDataPOS(){

        double [][] dataArray = new double[][]
                {
                        convert(klasterL1.klastry[0][0].colory),
                        convert(klasterL2.klastry[0][0].colory),
                        convert(klasterL3.klastry[0][0].colory),
                        convert(klasterL4.klastry[0][0].colory),
                        convert(klasterL5.klastry[0][0].colory),
                        convert(klasterL6.klastry[0][0].colory),
                        convert(klasterL7.klastry[0][0].colory),
                        convert(klasterL8.klastry[0][0].colory),
                        convert(klasterL9.klastry[0][0].colory),
                        convert(klasterL10.klastry[0][0].colory),
                        convert(klasterL11.klastry[0][0].colory),
                        convert(klasterL12.klastry[0][0].colory),
                        convert(klasterL13.klastry[0][0].colory)

                };


        DataSet dataSet = new DataSet(625, 1);

        for(int i = 0; i < dataArray.length; i++){

                dataSet.addRow(dataArray[i], new double[]{1});

        }

        return dataSet;
    }

    public DataSet getTrainingSet(){

        double[][] Array = new double[][]
                {
                        convert(KTS1.klastry[0][0].colory),
                        convert(KTS2.klastry[0][0].colory),
                        convert(KTS3.klastry[0][0].colory),
                        convert(KTS4.klastry[0][0].colory),
                        convert(KTS5.klastry[0][0].colory),
                        convert(KTS6.klastry[0][0].colory),
                        convert(KTS7.klastry[0][0].colory),
                        convert(KTS8.klastry[0][0].colory),
                        convert(KTS9.klastry[0][0].colory),
                        convert(KTS10.klastry[0][0].colory),

                };
                showArray(convert(KTS1.klastry[0][0].colory));

        DataSet dataSet = new DataSet(144, 10);

        dataSet.addRow(Array[0], new double[]{1,0,0,0,0,0,0,0,0,0});
        dataSet.addRow(Array[1], new double[]{0,1,0,0,0,0,0,0,0,0});
        dataSet.addRow(Array[2], new double[]{0,0,1,0,0,0,0,0,0,0});
        dataSet.addRow(Array[3], new double[]{0,0,0,1,0,0,0,0,0,0});
        dataSet.addRow(Array[4], new double[]{0,0,0,0,1,0,0,0,0,0});
        dataSet.addRow(Array[5], new double[]{0,0,0,0,0,1,0,0,0,0});
        dataSet.addRow(Array[6], new double[]{0,0,0,0,0,0,1,0,0,0});
        dataSet.addRow(Array[7], new double[]{0,0,0,0,0,0,0,1,0,0});
        dataSet.addRow(Array[8], new double[]{0,0,0,0,0,0,0,0,1,0});
        dataSet.addRow(Array[9], new double[]{0,0,0,0,0,0,0,0,0,1});


        return dataSet;
    }

    public DataSet getPOSITIV(){

        double[][] Array = new double[][]
                {
                        convert(KTS1.klastry[0][0].colory)

                };

        //showArray(convert(KTS1.klastry[0][0].colory));

        DataSet dataSet = new DataSet(144, 1);

        dataSet.addRow(Array[0], new double[]{1});




        return dataSet;
    }

    public DataSet getValidationSet(){

        double[][] Array = new double[][]
                {
                        convert(KTW1.klastry[0][0].colory),
                        convert(KTW2.klastry[0][0].colory),
                        convert(KTW3.klastry[0][0].colory),
                        convert(KTW4.klastry[0][0].colory)

                };
        DataSet dataSet = new DataSet(144, 10);

        dataSet.addRow(Array[0], new double[]{0,0,0,0,0,0,0,0,0,1});
        dataSet.addRow(Array[1], new double[]{0,0,0,0,0,1,0,0,0,0});
        dataSet.addRow(Array[2], new double[]{0,0,0,0,1,0,0,0,0,0});
        dataSet.addRow(Array[3], new double[]{0,0,0,0,0,0,0,0,1,0});


        return dataSet;
    }


}
