package Source;

import Adaline_PSI2.Adaline_PSI2;
import Back_Prop_PSI3.Back_Prop_PSI3;
import Cohenen_PSI_5.Cohenen_PSI_5;
import Hebb_PSI4_1.Hebb_PSI4_1;
import Hopfield_PSI_6.Hopfield_PSi_6;
import Ojj_PSI4_2.Ojj_PSI4_2;
import Perceptron_PSI1.Perceptron_1;
import org.neuroph.core.data.DataSet;

import java.io.IOException;

/**
 * Created by HP on 2017-01-21.
 */
public class AppPSI {





    public static void main(String[] argv) throws IOException {


        System.out.println("Perceptron");

        //Perceptron_1 zagadnienie_1 = new Perceptron_1();

        System.out.println("Adaline");

       // Adaline_PSI2 zagadnienie_2 = new Adaline_PSI2();

        System.out.println("Wsteczna propagacja");

       // Back_Prop_PSI3 zagadnienie_3 = new Back_Prop_PSI3();

        System.out.println("Hebb z nauczycielem");

        Hebb_PSI4_1 zagadnienie_4_1 = new Hebb_PSI4_1();

        zagadnienie_4_1.Hebb_Teacher();

        System.out.println("Hebb bez nauczycielem");

        //zagadnienie_4_1.Hebb_wTeacher();

        System.out.println("Ojj");

        //Ojj_PSI4_2 zagadnienie_4_2 = new Ojj_PSI4_2();

        //zagadnienie_4_2.initOja();

        System.out.println("Cohenen");

        //Cohenen_PSI_5 zagadnienie_5 = new Cohenen_PSI_5();
        //zagadnienie_5.initKohenen();

        System.out.println("Hopfield");

        //Hopfield_PSi_6 zagadnienia_6 = new Hopfield_PSi_6();

    }
}
