package Perceptron_PSI1;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Random;

public class Perceptron {
    static int liczba_epok = 99;
    static double szybkosc_uczenia = 0.001;
    static int liczba_danych = 100;
    static int theta = 0;

    public static void main(String args[]) throws FileNotFoundException{
        double[] x = new double[liczba_danych];
        double[] y = new double[liczba_danych];
        double[] z = new double[liczba_danych];
        int[] outputs = new int[liczba_danych];
        boolean var;

        Random generator = new Random();
        for(int i = 0; i < liczba_danych;i++){
            var = generator.nextBoolean();
            if(var == true){
                x[i] = randomNumber(-2 , 5);
                y[i] = randomNumber(-2 , 5);
                z[i] = randomNumber(-2 , 5);
                outputs[i] = 0;
                //System.out.println(x[i]+"\t\t"+y[i]+"\t\t"+z[i]+"\t\t"+outputs[i]);
            }else if(var == false){
                x[i] = randomNumber(-5 ,2);
                y[i] = randomNumber(-5, 2);
                z[i] = randomNumber(-5, 2);
                outputs[i] = 1;
                //System.out.println(x[i]+"\t\t"+y[i]+"\t\t"+z[i]+"\t\t"+outputs[i]);
            }

        }
        Random varD = new Random();
        double[] weights = new double[4];
        float localError, globalError;
        int epoki, output;

        weights[0] = varD.nextDouble(); //w1
        weights[1] = varD.nextDouble(); //w2
        weights[2] = varD.nextDouble(); //w3
        weights[3] = varD.nextDouble(); //bias

        epoki = 0;
        PrintWriter save = new PrintWriter("data.txt");
        PrintWriter errros = new PrintWriter("error.txt");

        long start = System.currentTimeMillis();
        do{
            epoki++;
            globalError = 0;
            String validacja = "";
            int so_bad = 0;
            int perfect = 0;
            int so_good = 0;


            for(int p = 0; p < liczba_danych - 30; p++){

                output = calculateOutput(theta, weights, x[p], y[p], z[p]);
                    localError = outputs[p] - output;
                if(localError == 0){
                    perfect++;
                }else if( localError < 0){
                    so_bad++;
                }else{
                    so_good++;
                }
                //save.println(localError);
                    weights[0] += szybkosc_uczenia * localError * x[p];
                    weights[1] += szybkosc_uczenia * localError * y[p];
                    weights[2] += szybkosc_uczenia * localError * z[p];
                    weights[3] += szybkosc_uczenia * localError;
                    globalError += (localError * localError);
                    //MAPE += localError/outputs[p];
            }
            errros.println(so_bad+"\t;\t"+perfect+"\t;\t"+so_good);
            validacja = VALIDACJA(weights, x, y, z, outputs);

            //MSE?
            System.out.println("epoki "+epoki+" : MSE = "+(globalError/ liczba_danych));
            save.println(globalError/(liczba_danych - 30) +"\t;\t"+ validacja /*+"\t;\t" + MAPE/(liczba_danych-30)*/);


        }while (globalError != 0 && epoki <= liczba_epok);

        long stop = System.currentTimeMillis();

        errros.close();
        save.close();
        System.out.println("Czas wykonania : "+(stop - start));
        System.out.println("\n========\n");
        System.out.println(weights[0] +"*x + "+weights[1]+"*y +  "+weights[2]+"*z +  "+weights[3]+" = 0");

/*
        for(int j = 0; j < 10; j++){
            double x1 = randomNumber(-9 , 0);
            double y1 = randomNumber(-9, 0);
            double z1 = randomNumber(-9 , 0);

            output = calculateOutput(theta,weights, x1, y1, z1);

            System.out.println("\n=======\nNew Random Point:");
            System.out.println("x = "+x1+" ,y = "+y1+" ,z = "+z1);
            System.out.println("class = "+output);
        }
*/
    }

    public static double randomNumber(double min, double max){
        DecimalFormat df = new DecimalFormat("#,####");
        double d = min + Math.random() * (max - min);
        String s = df.format(d);
        double x = Double.parseDouble(s);
        return x;
    }

    public static int calculateOutput(int theta, double weights[], double x, double y, double z){
        double sum = x * weights[0] + y * weights[1] + z *weights[2] + weights[3];
        return (sum >= theta) ? 1 : 0;
    }
    public static String VALIDACJA(double weights[], double x[], double y[], double z[], int outputs[]){
        double globalVAl = 0;
        int liczba_val = 0;
        for(int i = 71; i<liczba_danych; i++) {

            double sum = x[i] * weights[0] + y[i] * weights[1] + z[i] * weights[2] + weights[3];
            globalVAl += (outputs[i] - sum)*(outputs[i] - sum);
            liczba_val++;
        }
        System.out.println("Blad walidacji =="+globalVAl/liczba_val);
        return ""+globalVAl/liczba_val;
    }

}
