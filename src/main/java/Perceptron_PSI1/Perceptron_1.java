package Perceptron_PSI1;

import Source.Input;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Perceptron;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by HP on 2017-01-20.
 */
public class Perceptron_1 {

    public static Input data;
    public static double[] DataTrue1,DataTrue2,DataFalse1,DataFalse2,DataConfrim1;

    public Perceptron_1() throws IOException {

        data = new Input();

        DataTrue1 = data.convert(data.klasterL4.klastry[0][0].colory);
        DataTrue2 = data.convert(data.klasterL6.klastry[0][0].colory);
        DataFalse1 = data.convert(data.klasterS2.klastry[0][0].colory);
        DataFalse2 = data.convert(data.klasterS4.klastry[0][0].colory);
        DataConfrim1 = data.convert(data.klasterL7.klastry[0][0].colory);




        DataSet trainingSet = new DataSet(625, 1);

        trainingSet.addRow(new DataSetRow(DataTrue1, new double[]{1}));
        trainingSet.addRow(new DataSetRow(DataTrue2, new double[]{1}));
        trainingSet.addRow(new DataSetRow(DataFalse1, new double[]{0}));
        trainingSet.addRow(new DataSetRow(DataFalse2, new double[]{0}));


        DataSet ValidSet = new DataSet(625, 1);
        ValidSet.addRow(DataFalse1, new double[]{0});
        ValidSet.addRow(DataConfrim1, new double[]{1});

        NeuralNetwork myPerceptron = new org.neuroph.nnet.Perceptron(625, 1);

        myPerceptron.learn(trainingSet);

        System.out.println("Testing trained perceptron");
        testNeuralNetwork(myPerceptron, trainingSet);

        myPerceptron.save("mySamplePerceptron.nnet");

        NeuralNetwork loadedPerceptron = NeuralNetwork.load("mySamplePerceptron.nnet");
        System.out.println("Testing loaded perceptron");
        testNeuralNetwork(loadedPerceptron, trainingSet);

        System.out.println("Sprawdzenie wyniku");
        testNeuralNetwork(loadedPerceptron, ValidSet);


    }

    public static void testNeuralNetwork(NeuralNetwork neuralNet, DataSet testSet) {

        for (DataSetRow trainingElement : testSet.getRows()) {
            neuralNet.setInput(trainingElement.getInput());
            neuralNet.calculate();
            double[] networkOutput = neuralNet.getOutput();

            System.out.print("Input: " + Arrays.toString(trainingElement.getInput()));
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }


    }
}
