package Back_Prop_PSI3;

import Source.Input;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;

import java.io.IOException;
import java.io.SyncFailedException;
import java.util.Arrays;

/**
 * Created by HP on 2017-01-21.
 */
public class Back_Prop_PSI3 {

    private BackPropagation backpropagation;
    private MultiLayerPerceptron MultiLayerNetwork;
    private Input data = new Input();
    double start = 0, stop = 0 ;
    double execute;


    public Back_Prop_PSI3() throws IOException {


        MultiLayerNetwork = new MultiLayerPerceptron(144, 70, 10);

        backpropagation = MultiLayerNetwork.getLearningRule();
        backpropagation.addListener(new LearningEventListener() {

            public void handleLearningEvent(LearningEvent event) {
                BackPropagation bp = (BackPropagation) event.getSource();
                System.out.println("Epoch: " + bp.getCurrentIteration() + " MSE: " + bp.getTotalNetworkError());
            }
        });
        backpropagation.setLearningRate(0.001);
        backpropagation.setMinErrorChange(0.01);
        backpropagation.setMaxIterations(5000);

        start = System.currentTimeMillis();
        MultiLayerNetwork.learn(data.getTrainingSet());

        stop = System.currentTimeMillis();

        execute = stop -start;

        System.out.println("CZAS TRWANIA :" + execute);

        validateBackpropagation();
    }


    private void validateBackpropagation() {

        for (DataSetRow dataRow : data.getValidationSet().getRows()) {

            MultiLayerNetwork.setInput(dataRow.getInput());
            MultiLayerNetwork.calculate();

            double[] networkOutput = MultiLayerNetwork.getOutput();
            System.out.println("Input: ");
            data.showArray(dataRow.getInput());
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }
        System.out.println("Execution time: " + execute+ " ms");
        System.out.println();
    }


}

