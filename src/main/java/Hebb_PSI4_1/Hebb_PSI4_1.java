package Hebb_PSI4_1;

import Source.Input;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.SupervisedHebbianNetwork;
import org.neuroph.nnet.UnsupervisedHebbianNetwork;
import org.neuroph.nnet.learning.SupervisedHebbianLearning;
import org.neuroph.nnet.learning.UnsupervisedHebbianLearning;

import java.io.IOException;

/**
 * Created by HP on 2017-01-21.
 */
public class Hebb_PSI4_1 {

    public Input data = new Input();

    public void Hebb_Teacher(){

        SupervisedHebbianNetwork superHebb = new SupervisedHebbianNetwork(144, 10);

        SupervisedHebbianLearning S_H = new SupervisedHebbianLearning();
        S_H.addListener(new LearningEventListener() {
            public void handleLearningEvent(LearningEvent learningEvent) {
                SupervisedHebbianLearning S_H  =  (SupervisedHebbianLearning) learningEvent.getSource();
                System.out.println("Epoch: " + S_H.getCurrentIteration() + " MSE: " + S_H.getTotalNetworkError());
            }
        });

        S_H.setMaxIterations(5000);
        //S_H.setMinErrorChange(0.1);
        superHebb.setLearningRule(S_H);

        double start = System.currentTimeMillis();
        superHebb.learn(data.getTrainingSet());
        double stop = System.currentTimeMillis();

        System.out.println("Czas trwania = " + (stop-start));
    }
/*
    private void validateSupervisedHebbian() {
        System.out.println();
        for (DataSetRow dataRow : data.getValidatingSet().getRows()) {

            supervisedHebianNetwork.setInput(dataRow.getInput());
            supervisedHebianNetwork.calculate();

            double[] networkOutput = supervisedHebianNetwork.getOutput();
            System.out.println("Input: ");
            data.printMatrix(dataRow.getInput());
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }
        System.out.println("Execution time: " + executionTime + " ms");
        System.out.println();

    }

    private void validateUnsupervisedHebbian() {
        System.out.println();
        for (DataSetRow dataRow : data.getValidatingSet().getRows()) {

            unsupervisedHebianNetwork.setInput(dataRow.getInput());
            unsupervisedHebianNetwork.calculate();

            double[] networkOutput = unsupervisedHebianNetwork.getOutput();
            System.out.println("Input: ");
            data.printMatrix(dataRow.getInput());
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }
        System.out.println("Execution time: " + executionTime + " ms");
        System.out.println();

    }
   */
    public void Hebb_wTeacher(){

        UnsupervisedHebbianNetwork unsupervisedHebianNetwork = new UnsupervisedHebbianNetwork(144, 1);

        UnsupervisedHebbianLearning unsupervisedHebian = new UnsupervisedHebbianLearning();
	    unsupervisedHebian.addListener(new LearningEventListener() {


			public void handleLearningEvent(LearningEvent event) {
				UnsupervisedHebbianLearning unsupervisedHebbian = (UnsupervisedHebbianLearning) event.getSource();
				System.out.println("Epoka: " + unsupervisedHebbian.getCurrentIteration());
			}
		});

        unsupervisedHebianNetwork.setLearningRule(unsupervisedHebian);

        unsupervisedHebianNetwork.learn(data.getPOSITIV());



    }
    public Hebb_PSI4_1() throws IOException {

    }
}
