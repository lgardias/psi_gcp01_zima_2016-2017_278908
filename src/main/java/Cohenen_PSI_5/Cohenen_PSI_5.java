package Cohenen_PSI_5;

import Source.Input;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Kohonen;
import org.neuroph.nnet.learning.KohonenLearning;

import java.io.IOException;

/**
 * Created by HP on 2017-01-21.
 */
public class Cohenen_PSI_5 {

    private Input data = new Input();

    public Cohenen_PSI_5() throws IOException {
    }
/*
    public void validateKohenen() {
        System.out.println();
        for (DataSetRow dataRow : data.getPOSITIV().getRows()) {

            kohenen.setInput(dataRow.getInput());
            kohenen.calculate();

            double[] networkOutput = kohenen.getOutput();
            System.out.println("Input: ");
            data.printMatrix(dataRow.getInput());
            System.out.println(" Output: " + Arrays.toString(networkOutput));
        }
        System.out.println("Execution time: " + executionTime + " ms");
        System.out.println();

    }
*/
    public void initKohenen() {
        Kohonen kohenen= new Kohonen(144,10);
        KohonenLearning kohenenLearning = new KohonenLearning();
        kohenenLearning.setIterations(20, 20);


        kohenen.setLearningRule(kohenenLearning);


        kohenen.learn(data.getPOSITIV());

    }


}
