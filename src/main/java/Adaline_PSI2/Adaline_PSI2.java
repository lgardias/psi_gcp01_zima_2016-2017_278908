package Adaline_PSI2;

import Source.Input;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.Adaline;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.SigmoidDeltaRule;

import java.io.IOException;

/**
 * Created by HP on 2017-01-21.
 */
public class Adaline_PSI2 {


    private Adaline adaline;
    private SigmoidDeltaRule deltaRule;
    private static int numberImage =23;
    private Input data  = new Input();
    public double [][] dataArray;

    public Adaline_PSI2() throws IOException {


        DataSet dataSet = data.getTrainingSet();

        adaline = new Adaline(144);
        deltaRule = new SigmoidDeltaRule();
        deltaRule.addListener(new LearningEventListener() {
            public void handleLearningEvent(LearningEvent event) {
                SigmoidDeltaRule delta = (SigmoidDeltaRule) event.getSource();
                System.out.println("Epoch: " + delta.getCurrentIteration() + " MSE: " + delta.getTotalNetworkError());
            }
        });

        deltaRule.setMaxIterations(5000);
        adaline.setLearningRule(deltaRule);

        adaline.learn(dataSet);

    }

}

